// Nombre:
// Fecha:

#include <opencv2/highgui/highgui.hpp>
#include <opencv/cv.h>
#include <iostream>

using namespace cv;
using namespace std;

int main(int argc,char ** argv)
{
    IplImage *img = cvLoadImage("imgverde.png");
    IplImage *img1 = cvCloneImage(img);
    IplImage *img2 = cvCloneImage(img);
    
    IplConvKernel* kernel = 
            cvCreateStructuringElementEx
            (
                5, //cols
                5, //rows
                2, //anchor x
                2, //anchor y
                MORPH_ELLIPSE //forma
            );

    cvDilate
    (
        img,    // source image
        img1,   // destination image
        kernel, // structuring element
        1       // number of iterations
    );

    cvErode
    (
        img1,   // source image
        img2,   // destination image
        kernel, // structuring element
        1       // number of iterations
    );

    
    cvNamedWindow("Imagen:",1);  
    cvShowImage("Imagen:",img);  

    cvNamedWindow("Imagen dilatada:",1);  
    cvShowImage("Imagen dilatada:",img1);

    cvNamedWindow("Imagen cerrada:",1);  
    cvShowImage("Imagen cerrada:",img2);

    cvWaitKey();                // Espera a que se presione una tecla
    
    cvDestroyWindow("Imagen:");	// Cierra ventana "Image:"
    cvDestroyWindow("Imagen cerrada:");
    cvDestroyWindow("Imagen dilatada:");
    
    cvReleaseStructuringElement(&kernel);
    cvReleaseImage(&img);       // Libera la memoria ocupada por img
    cvReleaseImage(&img1);      // Libera la memoria ocupada por img1
    cvReleaseImage(&img2);      // Libera la memoria ocupada por img2

    return 0; 
}
